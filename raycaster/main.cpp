#include <SDL.h>

#define WIDTH   320
#define HEIGHT  200

typedef unsigned char ubyte;

unsigned int convertPixel(ubyte p)
{
    p = (ubyte)((p - 16) * 16);
    return p | (p << 8) | (p << 16) | 0xff000000;
}

void render(SDL_Renderer* ren, SDL_Texture* tex)
{
    unsigned char fb[65536];
    unsigned char data[] = {0xCD, 0x20, 0xFF, 0x9F};
    
    unsigned char mouseButton = 0xff;
    unsigned char mouseX  = 0xff;
    unsigned char mouseY  = 0xff;
    unsigned short idx = 0xfffe;
    
    ubyte  stepsLeft;           
    ubyte  z;                   
    ushort offsetInFrame;       
    ushort width;               
    ushort y;                   
    ushort x;                   
    ubyte  texCoord;            
    ubyte  texel;               
    uint   temp;                
    uint   temp2;
    
nextframe:
    stepsLeft = 255;
nextstep:
    z = 255 - stepsLeft;
    offsetInFrame = (ushort)(idx - 16);
    width = WIDTH;
    
    temp = offsetInFrame;
    y = (ushort)(temp / width);
    x = (ushort)(temp % width);
    x = (ushort)((x - 100) * z + 0x13b0);
    y = (ushort)((y - 100) * z + 0x13b0);
    x >>= 8;                    
    y >>= 8;
    
    if (mouseButton & 7)
    {
        x += mouseX;            
        z -= mouseY;
    }
    else
    {
        ubyte ticks = (ubyte)(SDL_GetTicks()/60);
        z -= 0xb0;
        if ((ticks & 0x40) == 0)
            x += ticks;
        z += ticks;
    }
    
    temp2 = z >> 6;
    x -= data[temp2];           
    texCoord = (ubyte)x;        
    x &= z;                     
    x |= y;                     
    x &= 0x20;                  
    if (stepsLeft-- && x == 0)
        goto nextstep;
    
    texel = texCoord ^ z;         
    texel = texel ^ (ubyte)y;     
    texel %= 16;                  
    texel += 16;
    fb[idx++] = texel;
    
    if (idx != 0)
        goto nextframe;
    
    SDL_Event event;
    if (SDL_PollEvent(&event))
    {
        mouseButton = (event.type == SDL_MOUSEBUTTONDOWN) ? 2 : 0;
        if (mouseButton)
        {
            mouseX = (ubyte)event.button.x;
            mouseY = (ubyte)event.button.y;
        }
        
        if (event.type == SDL_KEYUP && event.key.keysym.sym == SDLK_ESCAPE)
            return;
    }
    
    unsigned int* subarray = new unsigned int[16+WIDTH*HEIGHT];
    
    // (fb[16..(16+320*200)])
    
    for (auto i = 16; i < (16+WIDTH*HEIGHT); i++)
    {
        subarray[(i-16)] = convertPixel(fb[i]);
    }
    
    SDL_UpdateTexture(tex, NULL, subarray, WIDTH * sizeof(unsigned int));
    
    delete[] subarray;
    
    SDL_RenderCopy(ren, tex, NULL, NULL);
    SDL_RenderPresent(ren);
    goto nextframe;
}

int main(int argc, char **argv)
{
    SDL_Renderer *ren;
    SDL_Window *win;
    
    SDL_Init(SDL_INIT_VIDEO);
    
    SDL_CreateWindowAndRenderer(WIDTH, HEIGHT, 0, &win, &ren);
    SDL_Texture* tex = SDL_CreateTexture(ren, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, WIDTH, HEIGHT);
    
    render(ren, tex);
    
    SDL_Quit();
    
    return 0;
}